---
title: "README"
author: "Gergo David"
output: html_document
---
## Song Recommender
---

This is the README file of my song recommender program. It uses the **"Spotify Dataset 1921-2020, 160+ k Tracks"** by Yamaç Eren Ay, and the **Spotify API** and it's R wrapper, "spotifyr".
  
You can acquire the dataset at the following address:  
 **[Kaggle Dataset](https://www.kaggle.com/yamaerenay/spotify-dataset-19212020-160k-tracks)**  
 You can create your own Spotify app by creating a Spotify Developer account:  
 **[Spotify Developer Account](https://developer.spotify.com/documentation/web-api/)**  

## The goal of the program:

This app is used in my BSc thesis, which aims to show the wide range of applications for statistics software, like the R language and its packages. In my opinion, with R and Shiny, you can create powerful apps with ease, and also visualize data in an intuitive manner.

The basic idea behind the app was to create a music recommender system, which uses the song attributes given by Spotify. These attributes are the following (The ones I've used):

- #### One-hot attributes

  - **Mode**
  - **Explicit**
  - **Liveness**

- #### Attributes about a song's structure, mood, and overall sound

  - **Key**
  - **Tempo**
  - **Energy**
  - **Valence**
  - **Loudness**
  - **Speechiness**
  - **Danceability**
  - **Accousticness**
  - **Instrumentalness**

- #### Attributes that do not directly affect the sound of a track

  - **Popularity**
  - **Year of release**


Most of these attributes have descriptive names, the ones worth explaining might be **Valence**, which shows the musical positivity of the songs, tracks with higher valence sound more cheerful. **Mode** means if the song is played in a major or minor key. **Liveness** is supposed to be a binary value, it ranges from zero to one, but Spotify recommends you convert it to one if it's value is over 0.8, and to zero, if it's under. The **Year of release** and **Popularity** features do not describe the sound of the track, but in my opinion, they were important for giving a user tracks they might be familiar with already.

The user recommendations on Spotify are supposedly based on **other similar users' playlists**, **artists you listen to**, and **genres**. My goal was to throw the sonic attributes into this mix, and maybe compare the results of the different methods.  I used the K-Nearest-Neighbour algorithm on each datapoint of the dataset from Kaggle, so I could find out the 15 most sonically similar songs for each song.

The users can choose from the songs provided by the Kaggle dataset, or their own playlists from Spotify. They can choose from the songs and decide which to add to their new playlist. The app will return a mixed list of recommendations, which the users can upload to their Spotify account into a new playlist. They can also export their playlist into an Excel file. The app will also give the user a brief analysis of a chosen playlist, their favourite artist based on their Spotify account, and some visuals.

>#### To get the user's Spotify listening habits, they have to log into their accounts and give the app permission to pull their playlists and their top artists and songs.

### How to use the program's pages

#### 1st page: README:

This page shows a brief explanation of how the app works, the first time you use it it's worth a read, but otherwise, it has no other function.

#### 2nd page: Spotify login and analysis:

**The Spotify login should be done at the beginning, otherwise, you may lose data from your session with this program.**  
**The login is optional, if you do not have a Spotify account you can still browse music from this app!**  
The second page has a Spotify login button, by pressing it you get redirected to Spotify to sign in. After signing in you can choose your Spotify playlist to use, get your favorite artists and tracks in different time ranges and other visual data. 

**Examples of visual data:**  

<center>

<img src="fav_artists.png" width="60%" />  
**1. Image: User's favourite artists on medium time range.**  

<img src="playlist_data.png" width="60%" />  
**2. Image: A playlist's attributes.**    

</center>
  
#### 3rd page: Popular songs from the dataset:

When you load the page, the app creates an empty playlist for you, which you can fill up by using the app.
The second page contains **the most popular songs from the dataset**. One thousand songs are shown, ten of them at a time, on generated buttons, and of course randomly. When you click on a button, the song gets added to your current playlist automatically. The "Give me other songs" button empties the current set of buttons and creates buttons for the next ten songs. After 1000 songs, these buttons get disabled but don't worry, on the next page you can find every song from the dataset.

The popularity of the songs is determined by the number of plays on Spotify, so the songs not only contain the newest most popular songs, but also some older classics.

#### 4th page: All songs from the dataset, on a data table:

You can find every song from the dataset on the fourth page. It contains a data table, which shows a song's name, artists, and year of release. It's a pretty basic interface for searching and subsetting the data. Here you can add tracks to your temporary playlist.

#### 5th page: All songs from your playlist used in the app:

This page is very similar to the last one, except it contains your temporary playlist, created on the previous and following pages. There is also a button, which lets you upload the playlist to your Spotify account and buttons that delete data from the playlist.

#### 6th page: A page for the recommendations:

On the last page, there is a button, which on press gets recommendations for your songs. You can choose the number of recommendations you want to see for each song, this number ranges between one and fifteen. The layout is still similar, it has a data table, and you can add tracks from here to your playlist. By pressing the button again you can get recommendations for newly added songs. When the app gets your Spotify playlists, you can build those tracks into your recommendations too, and also create your recommendations as a playlist on Spotify. If you see "This app (estimated)" on the recommendations datatable, it means your track wasn't in the Kaggle dataset. You still get a recommendation but it might not be as good of a match as other ones.
  
#### Thank you for using the app, have fun!